沙玉妍 软件工程三班202010414315

***\*一、项目介绍\****

Oracle数据库商品销售系统是一个基于Oracle数据库的综合性销售管理系统，旨在帮助企业高效地管理和追踪商品销售活动。该系统提供了一套完整的功能模块，涵盖了从商品管理到销售订单处理的全过程。可以帮助企业有效管理商品销售过程，提高销售效率和客户满意度。通过该系统，企业可以实现以下目标：

\1. 提高销售效率：系统通过自动化的销售流程和订单处理，减少了繁琐的手工操作，提高了销售人员的工作效率。销售人员可以快速创建订单、查询商品信息，实时更新库存情况，从而更好地满足客户需求。

\2. 实时库存管理：系统通过实时更新库存数量和警报功能，帮助企业及时掌握商品库存情况。管理员可以根据库存报警信息进行补货和调整库存策略，避免库存过剩或缺货情况的发生，提高库存管理效率。

\3. 数据分析与决策支持：系统提供了丰富的报表和统计功能，管理员可以根据销售数据、客户行为等信息进行深入分析和挖掘，从而为企业决策提供有力支持。通过对销售趋势、热门产品、客户偏好等方面的分析，企业可以制定更有效的营销策略和销售计划。

\4. 数据安全与权限管理：系统通过严格的安全机制和权限管理，保护企业的数据安全。管理员可以设置用户角色和权限，限制不同用户的访问权限，确保敏感数据的保密性。此外，系统还提供数据备份和恢复功能，以防止数据丢失和灾难恢复。

总体而言，Oracle数据库商品销售系统是一个功能强大且灵活可定制的销售管理系统，帮助企业提升销售效率、优化库存管理、提供数据分析和决策支持，以及确保数据安全。通过该系统的应用，企业能够更好地管理和控制商品销售过程，提升竞争力并实现业务增长。

***\*二、\*\*实验\*\*要求\****

· 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。

o 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。

o 设计权限及用户分配方案。至少两个用户。

o 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。

o 设计一套数据库的备份方案。

三、实验步骤
***\*3.1 创建表空间\****

CREATE TABLESPACE system\_tablespace DATAFILE 'admin.dbf' SIZE 200M;

CREATE TABLESPACE user\_tablespace DATAFILE 'user.dbf' SIZE 200M;

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps1.jpg)

***\*3\*\*.2 创建表\****

\-- 创建用户表

CREATE TABLE users (

user\_id NUMBER PRIMARY KEY,

username VARCHAR2(50),

password VARCHAR2(50),

email VARCHAR2(100),

phone\_number VARCHAR2(20)

);

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps2.jpg)

\-- 创建商品表

CREATE TABLE products (

product\_id NUMBER PRIMARY KEY,

product\_name VARCHAR2(100),

product\_description VARCHAR2(500),

product\_price NUMBER,

product\_stock NUMBER

);

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps3.jpg)

\-- 创建订单表

CREATE TABLE orders (

order\_id NUMBER PRIMARY KEY,

user\_id NUMBER REFERENCES users(user\_id),

product\_id NUMBER REFERENCES products(product\_id),

order\_date DATE,

order\_quantity NUMBER,

order\_amount NUMBER

);

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps4.jpg)

\-- 创建库存表

CREATE TABLE inventory (

product\_id NUMBER PRIMARY KEY,

FOREIGN KEY (product\_id) REFERENCES products(product\_id),

current\_stock NUMBER

);

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps5.jpg)

***\*3.3 插入数据\****

\-- 向users表插入10万条数据

DECLARE

v\_counter NUMBER := 1;

BEGIN

WHILE v\_counter <= 100000 LOOP

INSERT INTO users (user\_id, username, email)

VALUES (v\_counter, 'User ' || v\_counter, 'user' || v\_counter || '@example.com');

v\_counter := v\_counter + 1;

END LOOP;

COMMIT;

END;

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps6.jpg)

\-- 向products表插入10万条数据

DECLARE

v\_counter NUMBER := 1;

BEGIN

WHILE v\_counter <= 100000 LOOP

INSERT INTO products (product\_id, product\_name, product\_description,product\_price,product\_stock)

VALUES (v\_counter, 'product\_name ' || v\_counter,'product\_description ' || v\_counter, v\_counter \* 10,v\_counter \* 100);

v\_counter := v\_counter + 1;

END LOOP;

COMMIT;

END;

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps7.jpg)

\-- 向orders表插入10万条数据

DECLARE

v\_counter NUMBER := 1;

BEGIN

WHILE v\_counter <= 100000 LOOP

INSERT INTO orders (order\_id, user\_id, product\_id, order\_quantity, order\_date,order\_amount)

VALUES (v\_counter, FLOOR(DBMS\_RANDOM.VALUE(1, 100000)), FLOOR(DBMS\_RANDOM.VALUE(1, 100000)), FLOOR(DBMS\_RANDOM.VALUE(1, 10)), SYSDATE,FLOOR(DBMS\_RANDOM.VALUE(1, 1000)));

v\_counter := v\_counter + 1;

END LOOP;

COMMIT;

END;

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps8.jpg)

\-- 向inventory表插入10万条数据

DECLARE

v\_counter NUMBER := 1;

BEGIN

WHILE v\_counter <= 100000 LOOP

INSERT INTO inventory (product\_id, current\_stock)

VALUES (v\_counter, FLOOR(DBMS\_RANDOM.VALUE(1, 1001)));

v\_counter := v\_counter + 1;

END LOOP;

COMMIT;

END;

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps9.jpg)

#### 3.4设计权限及用户分配

\-- 创建管理员用户

CREATE USER admin\_user IDENTIFIED BY pwd12345;

GRANT CONNECT, RESOURCE, DBA TO admin\_user;

\-- 创建普通用户

CREATE USER guest\_user IDENTIFIED BY 1234567;

GRANT CONNECT, RESOURCE TO guest\_user;

GRANT SELECT, INSERT, UPDATE, DELETE ON users TO guest\_user;

GRANT SELECT, INSERT, UPDATE, DELETE ON products  TO guest\_user;

GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO guest\_user;

GRANT SELECT, INSERT, UPDATE, DELETE ON inventory TO guest\_user;

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps10.jpg)

***\*3.5 PL/SQL设计\****

3.5.1创建程序包

\-- 创建程序包

CREATE OR REPLACE PACKAGE sales\_pkg AS

\-- 存储过程：创建订单

PROCEDURE create\_order(

p\_user\_id IN NUMBER,

p\_product\_id IN NUMBER,

p\_quantity IN NUMBER,

p\_order\_date IN DATE

);

\-- 存储过程：更新库存

PROCEDURE update\_inventory(

p\_product\_id IN NUMBER,

p\_quantity IN NUMBER

);

\-- 函数：计算订单总金额

FUNCTION calculate\_order\_amount(

p\_product\_price IN NUMBER,

p\_quantity IN NUMBER

) RETURN NUMBER;

END sales\_pkg;

/

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps11.jpg)

3.5.2函数设计

\-- 函数设计：创建订单

CREATE OR REPLACE PACKAGE BODY sales\_pkg AS

PROCEDURE create\_order(

p\_user\_id IN NUMBER,

p\_product\_id IN NUMBER,

p\_quantity IN NUMBER,

p\_order\_date IN DATE

) AS

v\_order\_id NUMBER;

v\_product\_price NUMBER;

v\_order\_amount NUMBER;

BEGIN

\-- 生成订单ID

SELECT order\_sequence.NEXTVAL INTO v\_order\_id FROM DUAL;

\-- 获取商品价格

SELECT product\_price INTO v\_product\_price FROM product WHERE product\_id = p\_product\_id;

\-- 计算订单总金额

v\_order\_amount := calculate\_order\_amount(v\_product\_price, p\_quantity);

\-- 插入订单记录

INSERT INTO order\_table (order\_id, user\_id, product\_id, order\_date, order\_quantity, order\_amount)

VALUES (v\_order\_id, p\_user\_id, p\_product\_id, p\_order\_date, p\_quantity, v\_order\_amount);

\-- 更新库存

update\_inventory(p\_product\_id, p\_quantity);

END create\_order;

PROCEDURE update\_inventory(

p\_product\_id IN NUMBER,

p\_quantity IN NUMBER

) AS

v\_current\_stock NUMBER;

BEGIN

\-- 获取当前库存

SELECT current\_stock INTO v\_current\_stock FROM inventory WHERE product\_id = p\_product\_id;

\-- 更新库存

v\_current\_stock := v\_current\_stock - p\_quantity;

\-- 更新库存表

UPDATE inventory SET current\_stock = v\_current\_stock WHERE product\_id = p\_product\_id;

END update\_inventory;

FUNCTION calculate\_order\_amount(

p\_product\_price IN NUMBER,

p\_quantity IN NUMBER

) RETURN NUMBER AS

v\_order\_amount NUMBER;

BEGIN

\-- 计算订单总金额

v\_order\_amount := p\_product\_price \* p\_quantity;

RETURN v\_order\_amount;

END calculate\_order\_amount;

END sales\_pkg;

/

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps12.jpg)

***\*3\*\*.6 备份方案\****

3.6.1.数据库备份脚本（backup\_script.sh）：

\#!/bin/bash

\# 设置Oracle环境变量

export ORACLE\_HOME=/path/to/oracle/home

export PATH=`$ORACLE_HOME/bin:$`PATH

export ORACLE\_SID=your\_oracle\_sid

\# 定义备份相关的变量

backup\_dir="/path/to/backup/directory"

backup\_date=\$(date +%Y%m%d%H%M%S)

backup\_log="`${backup_dir}/backup_$`{backup\_date}.log"

\# 创建备份目录（如果不存在）

mkdir -p \$backup\_dir

\# 使用RMAN进行备份

rman target / <&lt;EOF &gt;> \$backup\_log

\# 设置备份参数

CONFIGURE CONTROLFILE AUTOBACKUP ON;

CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '\${backup\_dir}/%F';

\# 执行备份

BACKUP DATABASE PLUS ARCHIVELOG;

\# 退出RMAN

EXIT;

EOF

\# 备份完成后，可以进行其他操作，如复制到远程服务器等

\# cp `${backup_dir}/backup_$`{backup\_date}.dmp remote\_server:/path/to/remote/location

\# 打印备份日志

cat \$backup\_log

![img](file:///C:\Users\NC069\AppData\Local\Temp\ksohtml14820\wps13.jpg)

3.6.2定期备份策略：

使用cron定时任务执行备份脚本，例如每周日凌晨进行全量备份。

3.6.3存储设备和位置：

将备份文件保存在指定的备份目录（/path/to/backup）中。

3.6.4备份周期和保留策略：

每周进行一次全量备份，保留最近30天的备份文件。

在备份脚本中使用find命令，删除30天前的备份文件。

3.6.5备份文件的安全性：

可以在脚本中添加加密备份文件的步骤，使用加密工具（如GPG）对备份文件进行加密。

3.6.6 监控和日志记录：

在备份脚本中添加日志记录，将备份操作的详细信息写入日志文件。

3.6.7灾难恢复策略：

根据需要，编写数据库恢复脚本，以从备份文件中还原数据库。

数据库备份在商品销售系统中至关重要，它提供了数据保护、业务连续性、灾难恢复、合规性和测试等多重好处，确保系统的可靠性、安全性和可用性，定期的备份策略是维护和管理数据库的重要组成部分。

***\*四、实验总结\****

在这个实验中，我们设计了一个基于Oracle数据库的商品销售系统的数据库设计方案。以下是总结：

\1. 数据库设计方案：

\- 创建了用户表（users），包含用户ID、用户名、密码、电子邮件和电话号码等字段。

\- 创建了商品表（products），包含商品ID、商品名称、商品描述、商品价格和商品库存等字段。

\- 创建了订单表（orders），包含订单ID、用户ID、商品ID、订单日期、订单数量和订单金额等字段。

\- 创建了库存表（inventory），包含商品ID和当前库存数量等字段。

\2. 权限及用户分配方案：

\- 创建了两个用户（admin\_user和guest\_user），并分配了CONNECT和RESOURCE权限。

\- 授权了用户对相应表的SELECT、INSERT、UPDATE和DELETE操作权限。

\3. 存储过程和函数设计：

\- 创建了程序包（sales\_pkg），其中包含了创建订单的存储过程（create\_order）、更新库存的存储过程（update\_inventory）以及计算订单总金额的函数（calculate\_order\_amount）。

\- 这些存储过程和函数实现了复杂的业务逻辑，通过调用它们可以实现订单的创建、库存的更新和订单金额的计算。

\4. 备份方案：

\- 在实际应用中，数据库的备份是非常重要的，以确保数据的安全性和可恢复性。

\- 可以使用Oracle数据库的备份工具（如RMAN）进行定期的数据库备份，并将备份文件存储在可靠的位置，以防止数据丢失或意外损坏。

通过完成这个实验，我们学习了如何设计一个基于Oracle数据库的商品销售系统，并了解了数据库的表设计、用户权限分配、存储过程和函数的使用，以及数据库备份的重要性。这些知识对于构建实际的商业应用系统非常有价值。
